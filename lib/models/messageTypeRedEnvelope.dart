import 'package:json_annotation/json_annotation.dart';

part 'messageTypeRedEnvelope.g.dart';

@JsonSerializable()
class MessageTypeRedEnvelope {
    MessageTypeRedEnvelope();

    String number;
    String formUserId;
    String toUserId;
    num tradePoints;
    String createTime;
    
    factory MessageTypeRedEnvelope.fromJson(Map<String,dynamic> json) => _$MessageTypeRedEnvelopeFromJson(json);
    Map<String, dynamic> toJson() => _$MessageTypeRedEnvelopeToJson(this);
}
