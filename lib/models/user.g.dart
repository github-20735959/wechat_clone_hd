// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) {
  return User()
    ..address = json['address'] as String
    ..company = json['company'] as String
    ..displayName = json['displayName'] as String
    ..email = json['email'] as String
    ..extra = json['extra'] as String
    ..friendAlias = json['friendAlias'] as String
    ..gender = json['gender'] as num
    ..groupAlias = json['groupAlias'] as String
    ..mobile = json['mobile'] as String
    ..name = json['name'] as String
    ..portrait = json['portrait'] as String
    ..social = json['social'] as String
    ..type = json['type'] as num
    ..uid = json['uid'] as String
    ..updateDt = json['updateDt'] as num;
}

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'address': instance.address,
      'company': instance.company,
      'displayName': instance.displayName,
      'email': instance.email,
      'extra': instance.extra,
      'friendAlias': instance.friendAlias,
      'gender': instance.gender,
      'groupAlias': instance.groupAlias,
      'mobile': instance.mobile,
      'name': instance.name,
      'portrait': instance.portrait,
      'social': instance.social,
      'type': instance.type,
      'uid': instance.uid,
      'updateDt': instance.updateDt
    };
