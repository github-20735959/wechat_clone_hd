import 'package:json_annotation/json_annotation.dart';

part 'loginResult.g.dart';

@JsonSerializable()
class LoginResult {
    LoginResult();

    String userId;
    String token;
    bool register;
    
    factory LoginResult.fromJson(Map<String,dynamic> json) => _$LoginResultFromJson(json);
    Map<String, dynamic> toJson() => _$LoginResultToJson(this);
}
