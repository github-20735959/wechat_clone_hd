import 'package:json_annotation/json_annotation.dart';

part 'user.g.dart';

@JsonSerializable()
class User {
    User();

    String address;
    String company;
    String displayName;
    String email;
    String extra;
    String friendAlias;
    num gender;
    String groupAlias;
    String mobile;
    String name;
    String portrait;
    String social;
    num type;
    String uid;
    num updateDt;
    
    factory User.fromJson(Map<String,dynamic> json) => _$UserFromJson(json);
    Map<String, dynamic> toJson() => _$UserToJson(this);
}
