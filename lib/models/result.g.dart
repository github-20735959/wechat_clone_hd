// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Result _$ResultFromJson(Map<String, dynamic> json) {
  return Result()
    ..appCode = json['appCode'] as String
    ..code = json['code'] as num
    ..message = json['message'] as String
    ..result = json['result'] == null
        ? null
        : LoginResult.fromJson(json['result'] as Map<String, dynamic>);
}

Map<String, dynamic> _$ResultToJson(Result instance) => <String, dynamic>{
      'appCode': instance.appCode,
      'code': instance.code,
      'message': instance.message,
      'result': instance.result
    };
