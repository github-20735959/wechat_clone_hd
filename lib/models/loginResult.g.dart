// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'loginResult.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResult _$LoginResultFromJson(Map<String, dynamic> json) {
  return LoginResult()
    ..userId = json['userId'] as String
    ..token = json['token'] as String
    ..register = json['register'] as bool;
}

Map<String, dynamic> _$LoginResultToJson(LoginResult instance) =>
    <String, dynamic>{
      'userId': instance.userId,
      'token': instance.token,
      'register': instance.register
    };
