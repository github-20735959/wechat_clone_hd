import 'package:json_annotation/json_annotation.dart';
import "loginResult.dart";
part 'result.g.dart';

@JsonSerializable()
class Result {
    Result();

    String appCode;
    num code;
    String message;
    LoginResult result;
    
    factory Result.fromJson(Map<String,dynamic> json) => _$ResultFromJson(json);
    Map<String, dynamic> toJson() => _$ResultToJson(this);
}
