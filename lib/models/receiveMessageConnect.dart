import 'package:json_annotation/json_annotation.dart';

part 'receiveMessageConnect.g.dart';

@JsonSerializable()
class ReceiveMessageConnect {
    ReceiveMessageConnect();

    String type;
    Map<String,dynamic> content;
    Map<String,dynamic> conversation;
    String messageId;
    String messageUid;
    String sender;
    String serverTime;
    String status;
    
    factory ReceiveMessageConnect.fromJson(Map<String,dynamic> json) => _$ReceiveMessageConnectFromJson(json);
    Map<String, dynamic> toJson() => _$ReceiveMessageConnectToJson(this);
}
