import 'package:json_annotation/json_annotation.dart';

part 'group.g.dart';

@JsonSerializable()
class Group {
    Group();

    String extra;
    num joinType;
    num memberCount;
    num mute;
    String name;
    String owner;
    String portrait;
    num privateChat;
    num searchable;
    String target;
    String type;
    num updateDt;
    
    factory Group.fromJson(Map<String,dynamic> json) => _$GroupFromJson(json);
    Map<String, dynamic> toJson() => _$GroupToJson(this);
}
