// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'messageTypeRedEnvelope.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageTypeRedEnvelope _$MessageTypeRedEnvelopeFromJson(
    Map<String, dynamic> json) {
  return MessageTypeRedEnvelope()
    ..number = json['number'] as String
    ..formUserId = json['formUserId'] as String
    ..toUserId = json['toUserId'] as String
    ..tradePoints = json['tradePoints'] as num
    ..createTime = json['createTime'] as String;
}

Map<String, dynamic> _$MessageTypeRedEnvelopeToJson(
        MessageTypeRedEnvelope instance) =>
    <String, dynamic>{
      'number': instance.number,
      'formUserId': instance.formUserId,
      'toUserId': instance.toUserId,
      'tradePoints': instance.tradePoints,
      'createTime': instance.createTime
    };
