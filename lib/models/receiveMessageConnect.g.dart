// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'receiveMessageConnect.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReceiveMessageConnect _$ReceiveMessageConnectFromJson(
    Map<String, dynamic> json) {
  return ReceiveMessageConnect()
    ..type = json['type'] as String
    ..content = json['content'] as Map<String, dynamic>
    ..conversation = json['conversation'] as Map<String, dynamic>
    ..messageId = json['messageId'] as String
    ..messageUid = json['messageUid'] as String
    ..sender = json['sender'] as String
    ..serverTime = json['serverTime'] as String
    ..status = json['status'] as String;
}

Map<String, dynamic> _$ReceiveMessageConnectToJson(
        ReceiveMessageConnect instance) =>
    <String, dynamic>{
      'type': instance.type,
      'content': instance.content,
      'conversation': instance.conversation,
      'messageId': instance.messageId,
      'messageUid': instance.messageUid,
      'sender': instance.sender,
      'serverTime': instance.serverTime,
      'status': instance.status
    };
