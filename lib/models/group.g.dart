// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'group.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Group _$GroupFromJson(Map<String, dynamic> json) {
  return Group()
    ..extra = json['extra'] as String
    ..joinType = json['joinType'] as num
    ..memberCount = json['memberCount'] as num
    ..mute = json['mute'] as num
    ..name = json['name'] as String
    ..owner = json['owner'] as String
    ..portrait = json['portrait'] as String
    ..privateChat = json['privateChat'] as num
    ..searchable = json['searchable'] as num
    ..target = json['target'] as String
    ..type = json['type'] as String
    ..updateDt = json['updateDt'] as num;
}

Map<String, dynamic> _$GroupToJson(Group instance) => <String, dynamic>{
      'extra': instance.extra,
      'joinType': instance.joinType,
      'memberCount': instance.memberCount,
      'mute': instance.mute,
      'name': instance.name,
      'owner': instance.owner,
      'portrait': instance.portrait,
      'privateChat': instance.privateChat,
      'searchable': instance.searchable,
      'target': instance.target,
      'type': instance.type,
      'updateDt': instance.updateDt
    };
