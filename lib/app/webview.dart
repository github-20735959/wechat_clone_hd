import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class MyWebView extends StatelessWidget {
  final String title;
  final String url;
  MyWebView({
    @required
    this.title, @required this.url});
  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: this.url,
      appBar: new AppBar(
        title: new Text(this.title),
      ),
    );
  }
}
