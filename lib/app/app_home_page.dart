import 'package:bai_le_men_im/app/webview.dart';
import 'package:flutter/material.dart';
import '../app/myAppBar.dart';

class AppHomePage extends StatefulWidget {
  @override
  _AppHomePageState createState() => _AppHomePageState();
}

class _AppHomePageState extends State<AppHomePage> {
  final GlobalKey<MyAppBarState> key = GlobalKey<MyAppBarState>();
  bool _isShowPop = false;

  List<String> coms = <String>["长沙数秩信息科技有限公司", "百乐门集团", "白沙集团"];

  String _currentCom = "长沙数秩信息科技有限公司";

  _startActivity() {}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MyAppBar(
          key: key,
          onShowPopHandler: () {
            this.setState(() {
              _isShowPop = true;
            });
          },
          onClosePopHandler: () {
            this.setState(() {
              _isShowPop = false;
            });
          }),
      // appBar: AppBar(
      //   centerTitle: false,
      //   leading: Container(),
      //   title: FlatButton(
      //     onPressed: () {},
      //     child: Text('长沙数秩信息科技有限公司'),
      //   ),
      // ),
      body: Stack(
        children: <Widget>[
          HomePageBody(companyName: _currentCom),
          _isShowPop
              ? Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Colors.grey.withOpacity(0.2),
                  child: Column(
                    children: coms.map((name) {
                      return Container(
                        padding: EdgeInsets.all(8.0),
                        color: Colors.white,
                        child: InkWell(
                          onTap: () {
                            this.setState(() {
                              _currentCom = name;
                            });
                            key.currentState.updateArrowStatus(name);
                          },
                          child: Row(
                            children: <Widget>[
                              Icon(Icons.access_alarms),
                              SizedBox(
                                width: 5.0,
                              ),
                              Expanded(
                                child: Text('$name'),
                              ),
                              _currentCom == name
                                  ? (Icon(
                                      Icons.check,
                                      color: Colors.green,
                                    ))
                                  : Container()
                            ],
                          ),
                        ),
                      );
                    }).toList(),
                  ),
                )
              : Container(),
        ],
      ),
    );
  }
}

class HomePageBody extends StatelessWidget {
  final String companyName;
  HomePageBody({this.companyName});
  _startActivity() {}
  @override
  Widget build(BuildContext context) {
    String logoImg;
    switch (companyName) {
      case '长沙数秩信息科技有限公司':
        logoImg =
            'http://up.deskcity.org/pic/9f/b0/d7/9fb0d7779107e15af7c30890e12967f1.jpg';
        break;
      case '百乐门集团':
        logoImg = 'https://photo.16pic.com/00/85/65/16pic_8565343_b.jpg';
        break;
      case '白沙集团':
        logoImg = 'https://photo.16pic.com/00/87/23/16pic_8723318_b.jpg';
        break;
      default:
    }
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 8.0),
      child: CustomScrollView(
        slivers: <Widget>[
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            AspectRatio(
              aspectRatio: 16.0 / 5.0,
              child: Container(
                child: Image.network(
                  logoImg,
                  fit: BoxFit.cover,
                ),
                decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.all(Radius.circular(5.0))),
              ),
            ),
            SizedBox(
              height: 12.0,
            ),
            Row(
              children: <Widget>[
                Icon(
                  Icons.notification_important,
                  color: Colors.grey,
                  size: 14.0,
                ),
                SizedBox(
                  width: 10.0,
                ),
                Expanded(
                    child: Text('置顶公告会在这里滚动通知',
                        style: TextStyle(color: Colors.grey, fontSize: 12.0))),
                InkWell(
                  onTap: () {},
                  child: Text('更多',
                      style: TextStyle(color: Colors.blue, fontSize: 12.0)),
                )
              ],
            ),
            SizedBox(
              height: 12.0,
            ),
            Divider(
              height: 0.0,
            ),
            SizedBox(
              height: 20.0,
            ),
            Text('常用应用',
                style: TextStyle(
                    // color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0)),
            SizedBox(
              height: 20.0,
            ),
          ])),
          SliverGrid.count(
            crossAxisCount: 4,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
            children: <Widget>[
              App(
                onTap: () {
                  Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => MyWebView(
                              title: '在线报表',
                              url:
                                  "http://powerBI:Power123@windev.digintech.cn/reports/powerbi/%E7%99%BE%E4%B9%90%E9%97%A8/%E7%99%BE%E4%B9%90%E9%97%A8%E6%89%8B%E6%9C%BA%E6%8A%A5%E8%A1%A8?rs:embed=true",
                            )),
                  );
                },
                iconImage: Container(
                  width: 30.0,
                  height: 30.0,
                  // color: Colors.red,
                  child: Center(
                    child: Image(
                      image: AssetImage("assets/images/powerBI.png"),
                      width: 30.0,
                      // width: 10.0,
                      // height: 10.0,
                      fit: BoxFit.cover,
                      // repeat: ImageRepeat.repeatY,
                      // fit: BoxFit.fitHeight,
                    ),
                  ),
                ),
                iconData: Icons.library_books,
                title: '在线报表',
                color: Color.fromRGBO(237, 188, 25, 1.0),
              ),
              App(
                iconData: Icons.movie,
                title: '电影',
                color: Colors.greenAccent,
              ),
              App(
                iconData: Icons.contact_phone,
                title: '联系人',
              ),
            ],
          ),
          SliverList(
              delegate: SliverChildListDelegate(<Widget>[
            SizedBox(
              height: 20.0,
            ),
            Divider(
              height: 0.0,
            ),
            SizedBox(
              height: 20.0,
            ),
            Text('其它应用',
                style: TextStyle(
                    // color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0)),
            SizedBox(
              height: 20.0,
            ),
          ])),
          SliverGrid.count(
            crossAxisCount: 4,
            crossAxisSpacing: 10.0,
            mainAxisSpacing: 10.0,
            children: <Widget>[
              App(
                onTap: _startActivity,
                iconData: Icons.library_books,
                title: '书籍',
                color: Colors.greenAccent,
              ),
              App(
                iconData: Icons.movie,
                title: '电影',
                color: Colors.greenAccent,
              ),
              App(
                iconData: Icons.fiber_new,
                title: '新闻',
                color: Colors.green,
              ),
              App(
                iconData: Icons.contact_phone,
                title: '联系人',
              ),
              App(
                iconData: Icons.library_books,
                title: '书籍',
                color: Colors.greenAccent,
              ),
              App(
                iconData: Icons.movie,
                title: '电影',
                color: Colors.redAccent,
              ),
            ],
          )
        ],
      ),
    );
  }
}

class App extends StatelessWidget {
  final Color color;
  final IconData iconData;
  final String title;
  final Function onTap;
  final Widget iconImage;
  App(
      {this.color = Colors.blue,
      this.iconData = Icons.location_city,
      this.title = '签到',
      this.iconImage,
      this.onTap});
  @override
  Widget build(BuildContext context) {
    Widget icon;
    if (this.iconImage != null) {
      icon = this.iconImage;
    } else {
      icon = Icon(
        this.iconData,
        color: Colors.white,
        size: 30.0,
      );
    }
    return InkWell(
      onTap: onTap,
      child: Container(
        child: Column(
          children: <Widget>[
            Expanded(
              child: AspectRatio(
                aspectRatio: 1.0 / 1.0,
                child: Container(
                  // width: 60.0,
                  // height: 60.0,
                  child: icon,
                  decoration: BoxDecoration(
                      color: color,
                      borderRadius: BorderRadius.all(Radius.circular(8.0))),
                ),
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Text(
              title,
              style: TextStyle(fontSize: 11.0),
            )
          ],
        ),
      ),
    );
  }
}
