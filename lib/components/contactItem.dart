import 'package:flutter/material.dart';

import '../constants.dart' show AppColors, AppStyles, Constants;

class ContactItem extends StatelessWidget {
  ContactItem({
    @required this.avatar,
    @required this.title,
    this.groupTitle,
    this.onPressed,
  });

  final String avatar;
  final String title;
  final String groupTitle;
  final VoidCallback onPressed;

  static const double MARGIN_VERTICAL = 8.0;
  static const double MARGIN_HORIZONTAL = 16.0;
  static const double GROUP_TITLE_HEIGHT = 34.0;

  bool get _isAvatarFromNet {
    return this.avatar.startsWith('http') || this.avatar.startsWith('https');
  }

  static double _height(bool hasGroupTitle) {
    final _buttonHeight = MARGIN_VERTICAL * 2 +
        Constants.ContactAvatarSize +
        Constants.DividerWidth;
    if (hasGroupTitle) {
      return _buttonHeight + GROUP_TITLE_HEIGHT;
    }
    return _buttonHeight;
  }

  @override
  Widget build(BuildContext context) {
    // 左边的图标
    Widget _avatarIcon;
    if (_isAvatarFromNet) {
      _avatarIcon = Image.network(
        avatar,
        width: Constants.ContactAvatarSize,
        height: Constants.ContactAvatarSize,
        fit: BoxFit.cover,
      );
    } else {
      _avatarIcon = Image.asset(
        avatar,
        width: Constants.ContactAvatarSize,
        height: Constants.ContactAvatarSize,
        fit: BoxFit.cover,
      );
    }
    _avatarIcon = ClipRRect(
      borderRadius: BorderRadius.circular(Constants.AvatarRadius),
      child: _avatarIcon,
    );

    // 列表项主体部分
    Widget _button = Container(
      margin: const EdgeInsets.only(left: MARGIN_HORIZONTAL),
      child: Row(
        children: <Widget>[
          _avatarIcon,
          SizedBox(width: 16.0),
          Expanded(
            child: Container(
              padding: const EdgeInsets.only(right: MARGIN_HORIZONTAL),
              height: ContactItem._height(false),
              alignment: Alignment.centerLeft,
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                      width: Constants.DividerWidth,
                      color: const Color(AppColors.DividerColor)),
                ),
              ),
              child: Text(title, style: AppStyles.TitleStyle, maxLines: 1),
            ),
          ),
        ],
      ),
    );

    // 分组标签
    Widget _itemBody;
    if (this.groupTitle != null) {
      _itemBody = Column(
        children: <Widget>[
          Container(
            height: GROUP_TITLE_HEIGHT,
            padding: EdgeInsets.only(left: 16.0, right: 16.0),
            color: const Color(AppColors.ContactGroupTitleBg),
            alignment: Alignment.centerLeft,
            child:
                Text(this.groupTitle, style: AppStyles.GroupTitleItemTextStyle),
          ),
          _button,
        ],
      );
    } else {
      _itemBody = _button;
    }

    return InkWell(
      onTap: onPressed,
      child: _itemBody,
    );
  }
}
