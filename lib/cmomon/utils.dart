import 'package:bai_le_men_im/pages/qrCodeScanner.dart';
import 'package:flutter/widgets.dart';

import '../constants.dart' show Constants;
import 'package:flutter/material.dart';

import 'package:permission_handler/permission_handler.dart';
import 'package:overlay_support/overlay_support.dart';

bool _isAvatarFromNet(avatar) {
  return avatar.startsWith('http') || avatar.startsWith('https');
}

Icon myGenderIcon(num gender, double size) {
  IconData iconData;
  Color color;
  switch (gender) {
    case 1:
      iconData = Icons.face;
      color = Colors.pinkAccent;
      break;
    default:
      iconData = Icons.perm_identity;
      color = Colors.blue;
      print(gender);
  }

  return Icon(
    iconData,
    size: size ?? 20.0,
    color: color,
  );
}

Widget myAvatar(String url, {double size = Constants.ContactAvatarSize}) {
  Widget avatar;
  String avatarStr = (url != null && url.isNotEmpty)
      ? url
      : 'assets/images/default_nor_avatar.png';
  if (_isAvatarFromNet(avatarStr)) {
    avatar = Image.network(
      avatarStr,
      width: size,
      height: size,
      fit: BoxFit.cover,
    );
  } else {
    avatar = Image.asset(
      avatarStr,
      width: size,
      height: size,
      fit: BoxFit.cover,
    );
  }
  return avatar;
}

void pushPageToQrCodeScanner(context) async {
  Map<PermissionGroup, PermissionStatus> permissions =
      await PermissionHandler().requestPermissions([
    PermissionGroup.camera,
  ]);
  List _permissions = List.generate(permissions.keys.length, (i) {
    return permissions[permissions.keys.toList()[i]] ==
        PermissionStatus.granted;
  });
  if (_permissions.every((_permission) {
    return _permission;
  })) {
    Navigator.of(context).push(new MaterialPageRoute(builder: (context) {
      return QrCodeScanner();
    }));
    return;
  } else {
    toast('获取摄像头权限失败');
  }
}

/// ListTile的右侧，元素加上一个右箭头
  Widget trailWidget(Widget child, {width: 100.0}) {
    return Container(
      width: width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[child, Icon(Icons.chevron_right)],
      ),
    );
  }
