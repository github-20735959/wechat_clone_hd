import 'package:flutter/foundation.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';
import '../models/index.dart';

import 'global.dart';

class UsersChangeNotifier extends ChangeNotifier {
  // Profile get _profile => Global.profile;

  @override
  void notifyListeners() {
    super.notifyListeners(); //通知依赖的Widget更新
  }
}

class UsersModel extends UsersChangeNotifier {
  List<User> _list = [];
  List<User> get list => _list ?? [];

  set list(List<User> list) {
    _list = list;
    notifyListeners();
  }
  Future<User> getUserInfoById (String userId) async {
    User user;
    int index = _list.indexWhere((data) => data.uid == userId);
    if (index > -1) {
      user = _list[index];
      if (user.displayName == null) {
        print('getUserInfoById');
        Map result = await FlutterWildfire.getUserInfo(userId, false);
        user = User.fromJson(result);
      }
    } else {
        print('getUserInfoById');
      Map result = await FlutterWildfire.getUserInfo(userId, false);
      user = User.fromJson(result);
      _list.add(user);
    }
    
    return user;
  }
}
