import 'package:flutter/foundation.dart';
import '../models/index.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';

import 'global.dart';

class ConversationChangeNotifier extends ChangeNotifier {
  // ConversationList get _list => Global.profile;

  @override
  void notifyListeners() {
    super.notifyListeners(); //通知依赖的Widget更新
  }
}

class ConversationModel extends ConversationChangeNotifier {
  List _list = [];
  List get list => _list ?? [];

  set list(List list) {
    _list.clear();
    _list.addAll(list);
    // _list = list;
    print('+++++++++++++++++++++++');
    notifyListeners();
    // if (user?.uid != _profile.user?.uid) {
    //   _profile.lastLogin = _profile.user?.uid;
    //   _profile.user = user;
    //   notifyListeners();
    // }
  }

  clearUnreadStatus (String _otherSideUserId) async {
    int index = _list.indexWhere((data) => data["conversation"]["target"] == _otherSideUserId && data["unreadCount"]["unread"] > 0);
      if (index > -1) {
        _list[index]["unreadCount"]["unread"] = 0;
        notifyListeners();
      }
      FlutterWildfire.clearUnreadStatus('Single', _otherSideUserId, 0);
    // await FlutterWildfire.clearUnreadStatus('Single', _otherSideUserId, 0);
    
  }
}
