import 'package:flutter/material.dart';

import '../constants.dart' show AppColors;

enum Device { MAC, WIN }

class Conversation {
  const Conversation(
      {@required this.avatar,
      @required this.otherSideUserId,
      @required this.title,
      this.displayName,
      this.type,
      this.titleColor: AppColors.TitleColor,
      this.des,
      @required this.updateAt,
      this.isMute: false,
      this.isTop: false,
      this.unreadMsgCount: 0,
      this.displayDot: false})
      : assert(avatar != null),
        assert(title != null),
        assert(updateAt != null);

  final String avatar;
  final String type;
  final String otherSideUserId;
  final String title;
  final int titleColor;
  final String des;
  final String updateAt;
  final bool isMute;
  final bool isTop;
  final int unreadMsgCount;
  final bool displayDot;
  final String displayName;

  bool isAvatarFromNet() {
    if (this.avatar.indexOf('http') == 0 || this.avatar.indexOf('https') == 0) {
      return true;
    }
    return false;
  }
}

class ConversationPageData {
  const ConversationPageData({
    this.device,
    this.conversations,
  });

  final Device device;
  final List<Conversation> conversations;

  static mock() {
    return ConversationPageData(
        device: Device.WIN, conversations: mockConversations);
  }

  static List<Conversation> mockConversations = [];
}
