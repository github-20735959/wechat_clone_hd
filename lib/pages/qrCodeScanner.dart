import 'package:bai_le_men_im/models/index.dart';
import 'package:bai_le_men_im/pages/userDetail.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:flutter/material.dart';
import 'package:overlay_support/overlay_support.dart';

class QrCodeScanner extends StatefulWidget {
  @override
  _QrCodeScannerState createState() => _QrCodeScannerState();
}

class _QrCodeScannerState extends State<QrCodeScanner> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  var qrText = "";
  QRViewController controller;
  OverlaySupportEntry overlay;

  showLoading () {
    overlay = showOverlay((context, t) {
      return Opacity(
        opacity: t,
        child: Material(
          color: Colors.black.withAlpha(80),
          child: Container(
            child: Center(
              child: Container(
                  width: 100.0,
                  height: 100.0,
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                      color: Colors.black.withAlpha(180),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: CircularProgressIndicator(
                    
                  )),
            ),
          ),
        ),
      );
    }, duration: Duration(microseconds: 0), key: const ValueKey('message'));
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) async {
      if (scanData is String) {
        if (scanData.startsWith('shuzhiim://')) {
          controller.pauseCamera();
          scanData = scanData.substring('shuzhiim://'.length);
          if (scanData.startsWith('user/')) {
            List<String> arr = scanData.split('/');
            if (arr.length > 1) {
              String userId = arr[1];
              showLoading();
              Map result = await FlutterWildfire.getUserInfo(userId, true);
              User user = User.fromJson(result);
              Navigator.of(context)
                  .pushReplacement(new MaterialPageRoute(builder: (context) {
                return UserDetailPage(userData: user);
              }));
            }
          }
        }
      }
      // setState(() {
      //   qrText = scanData;
      // });
    });
  }

  @override
  void dispose() {
    overlay?.dismiss();
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        iconTheme: Theme.of(context).iconTheme,
        title: Text(
          '二维码/条码',
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            flex: 5,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
            ),
          ),
          // Expanded(
          //   flex: 1,
          //   child: Center(
          //     child: Text('Scan result: $qrText'),
          //   ),
          // )
        ],
      ),
    );
  }
}
