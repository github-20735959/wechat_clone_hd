import 'package:flutter/material.dart';

class SearchAppBar extends StatefulWidget implements PreferredSizeWidget {
  final Function onkeywordControllerChangedHandler;
  SearchAppBar({Key key, this.onkeywordControllerChangedHandler})
      : super(key: key);

  @override
  Size get preferredSize {
    return new Size.fromHeight(56.0);
  }

  @override
  MyAppBarState createState() => MyAppBarState();
}

class MyAppBarState extends State<SearchAppBar> {
  TextEditingController keywordController;
  String direction = 'down';
  String _keyword;

  keywordControllerChangedHandler() async {
    final keyword = keywordController.text.trim();
    this.setState(() {
      _keyword = keyword;
    });
    widget.onkeywordControllerChangedHandler(keyword);
  }

  @override
  void initState() {
    super.initState();
    keywordController = TextEditingController();
    keywordController.addListener(keywordControllerChangedHandler);
  }

  _clickClearHandler() {
    keywordController.clear();
    this.setState(() {
      _keyword = null;
    });
    widget.onkeywordControllerChangedHandler('');
  }

  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      top: true,
      child: Container(
        // color: Color.fromRGBO(237, 237, 237, 1.0),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: <Widget>[
              // IconButton(
              //   icon: Icon(Icons.chevron_left),
              //   onPressed: (){},
              // ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 6.0),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(4.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 5.0),
                        child: Icon(
                          Icons.search,
                          color: Colors.grey,
                          size: 18.0,
                        ),
                      ),
                      Expanded(
                        child: TextField(
                          autofocus: true,
                          controller: keywordController,
                          decoration: InputDecoration(border: InputBorder.none),
                        ),
                      ),
                      (_keyword != null && _keyword.isNotEmpty)
                          ? InkWell(
                              onTap: _clickClearHandler,
                              child: Container(
                                padding: EdgeInsets.all(3.0),
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(142, 142, 147, 1.0),
                                    shape: BoxShape.circle),
                                child: Icon(
                                  Icons.close,
                                  color: Colors.white,
                                  size: 16.0,
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
                  // color: Colors.red,
                  child: Text(
                    '取消',
                    style: TextStyle(
                        fontSize: 18.0, color: Color.fromRGBO(97, 103, 139, 1)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  dispose() {
    keywordController.dispose();
    super.dispose();
  }
}
