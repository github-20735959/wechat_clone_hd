import 'package:async/async.dart';
import 'package:bai_le_men_im/cmomon/utils.dart';
import 'package:bai_le_men_im/models/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';
import '../constants.dart' show AppColors, AppStyles, Constants;

class RedEnvelopeDetail extends StatefulWidget {
  final MessageTypeRedEnvelope detail;
  RedEnvelopeDetail({this.detail});
  @override
  _RedEnvelopeDetailState createState() => _RedEnvelopeDetailState();
}

class _RedEnvelopeDetailState extends State<RedEnvelopeDetail> {
  final AsyncMemoizer _memoizerFrom = AsyncMemoizer();
  final AsyncMemoizer _memoizerTo = AsyncMemoizer();

  getFormUser() {
    return _memoizerFrom.runOnce(() async {
      String userId = widget.detail.formUserId;
      User result =
          User.fromJson(await FlutterWildfire.getUserInfo(userId, true));
      return result;
    });
  }

  getToUser() {
    return _memoizerTo.runOnce(() async {
      String userId = widget.detail.toUserId;
      User result =
          User.fromJson(await FlutterWildfire.getUserInfo(userId, true));
      return result;
    });
  }

  @override
  Widget build(BuildContext context) {
    Color bgColor = Colors.orange[900];

    final MessageTypeRedEnvelope detail = widget.detail;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: bgColor,
        elevation: 0.0,
        // title: Text('红包详情'),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 100.0,
              decoration: BoxDecoration(
                  color: bgColor,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(1300.0),
                      bottomLeft: Radius.circular(1300.0))),
              child: Center(
                child: FutureBuilder(
                    future: getFormUser(),
                    builder: (BuildContext context, AsyncSnapshot snapShop) {
                      if (!snapShop.hasData) {
                        return CircularProgressIndicator();
                      }
                      User user = snapShop.data;
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(Constants.AvatarRadius),
                              child: myAvatar(user.portrait)),
                              SizedBox(width: 10.0,),
                          Text(
                            user.displayName + '的积分红包',
                            style: TextStyle(
                                color: Colors.amber[400], fontSize: 20.0),
                          )
                        ],
                      );
                    }),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 10.0, top: 40.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '1个红包共' + detail.tradePoints.toInt().toString() + '个积分',
                    style: TextStyle(color: Colors.grey),
                  ),
                  // SizedBox(height: 10.0,),
                  Divider(
                      // height: 0.0,
                      ),
                  FutureBuilder(
                    future: getToUser(),
                    builder: (BuildContext context, AsyncSnapshot snapShop) {
                      if (!snapShop.hasData) {
                        return CircularProgressIndicator();
                      }
                      User user = snapShop.data;
                      return ListTile(
                        leading: ClipRRect(
                              borderRadius:
                                  BorderRadius.circular(Constants.AvatarRadius),
                              child: myAvatar(user.portrait)),
                        title: Text(user.displayName),
                        subtitle: Text(detail.createTime.substring(0, 16)),
                        trailing:
                            Text(detail.tradePoints.toInt().toString() + '个积分'),
                      );
                    }
                  ),
                  Divider(
                      // height: 0.0,
                      ),
                ],
              ),
            ),
            // Padding(
            //   padding: EdgeInsets.only(left: 10.0),
            //   child: Divider(
            //     height: 0.0,
            //   ),
            // )
          ],
        ),
      ),
    );
  }
}
