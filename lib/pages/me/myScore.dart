import 'package:flutter/material.dart';

class MyScorePage extends StatefulWidget {
  @override
  _MyScorePageState createState() => _MyScorePageState();
}

class _MyScorePageState extends State<MyScorePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        iconTheme: Theme.of(context).iconTheme,
        actions: <Widget>[
          // Text('积分明细')
        ],
      ),
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 30.0,
          ),
          CircleAvatar(
            backgroundColor: Colors.yellow[700],
            foregroundColor: Colors.white,
            child: Icon(Icons.account_balance_wallet, size: 50.0,),
            radius: 40.0,
          ),
          SizedBox(
            height: 30.0,
          ),
          
          Text('我的积分', textAlign: TextAlign.center,style: TextStyle(fontSize: 16.0, )),
          SizedBox(
            height: 20.0,
          ),
          Text('23723', style: TextStyle(color: Colors.black, fontSize: 45.0, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
        ],
      ),
    );
  }
}