import 'package:bai_le_men_im/cmomon/global.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';
import 'package:overlay_support/overlay_support.dart';
import '../../models/index.dart';
import '../../constants.dart' show Constants;
import 'package:provider/provider.dart';
import '../../cmomon/userModel.dart';

class ModifyMyNickName extends StatefulWidget {
  final String displayName;
  ModifyMyNickName({this.displayName});
  @override
  _ModifyMyNickNameState createState() => _ModifyMyNickNameState();
}

class _ModifyMyNickNameState extends State<ModifyMyNickName> {
  TextEditingController keywordController;
  bool _disabled = false;
  String keyword;

  keywordControllerChangedHandler() async {
    final _keyword = keywordController.text.trim();
    this.setState(() {
      keyword = _keyword;
      _disabled = keyword.isEmpty || (keyword == widget.displayName);
    });
  }

  @override
  void initState() {
    keyword = widget.displayName;
    keywordController = TextEditingController(text: widget.displayName);
    keywordController.addListener(keywordControllerChangedHandler);
    super.initState();
  }

  @override
  void dispose() {
    keywordController.removeListener(keywordControllerChangedHandler);
    keywordController.dispose();
    super.dispose();
  }

  saveNickNameHandler() async {
    try {
      final _keyword = keywordController.text.trim();
      await FlutterWildfire.modifyMyInfoNickName(_keyword);
    } catch (e) {
      var result = await FlutterWildfire.getUserInfo(Global.profile.user.uid, true);
      User user = User.fromJson(result);
      Global.profile.user = user;
      Navigator.of(context).pop();
      // toast('修改昵称错误');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          centerTitle: true,
          iconTheme: Theme.of(context).iconTheme,
          title: Text(
            '设置名字',
            style: TextStyle(color: Colors.black87),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: MaterialButton(
                disabledColor: Colors.green.withAlpha(80),
                minWidth: 50.0,
                onPressed: _disabled ? null : saveNickNameHandler,
                color: Colors.green,
                child: Text(
                  '完成',
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                ),
              ),
            )
          ],
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 12.0),
                child: Stack(
                  children: <Widget>[
                    TextField(
                      autofocus: true,
                      controller: keywordController,
                      decoration: InputDecoration(),
                    ),
                    keyword.isNotEmpty
                        ? (Positioned(
                            right: 0.0,
                            child: IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () {
                                keywordController.clear();
                              },
                            ),
                          ))
                        : Container()
                  ],
                ),
              )
            ],
          ),
        ));
  }
}
