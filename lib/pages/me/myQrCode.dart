import 'package:bai_le_men_im/cmomon/userModel.dart';
import 'package:bai_le_men_im/cmomon/utils.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import '../../constants.dart' show Constants;

class MyQrCode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        iconTheme: Theme.of(context).iconTheme,
        title: Text(
          '我的二维码',
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: Consumer<UserModel>(
          builder: (BuildContext context, UserModel userModel, Widget widget) {
            return Column(
              children: <Widget>[
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(children: <Widget>[
                      Row(children: <Widget>[
                        ClipRRect(
                          borderRadius:
                              BorderRadius.circular(Constants.AvatarRadius),
                          child: myAvatar(userModel.user.portrait, size: 60.0),
                        ),
                        SizedBox(width: 20.0),
                        Column(
                          children: <Widget>[Text(userModel.user.displayName)],
                        )
                      ]),
                      SizedBox(height: 30.0),
                      QrImage(
                        data: "shuzhiim://user/${userModel.user.uid}",
                        version: QrVersions.auto,
                        size: 200.0,
                      ),
                      SizedBox(height: 30.0),
                      Text(
                        '扫一扫上面的二维码图案，加好友',
                        style: TextStyle(color: Colors.grey),
                      ),
                    ]),
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
