import 'package:bai_le_men_im/cmomon/global.dart';
import 'package:bai_le_men_im/cmomon/utils.dart';
import 'package:bai_le_men_im/pages/me/modifyMyAvatar.dart';
import 'package:bai_le_men_im/pages/me/modifyMyNickName.dart';
import 'package:bai_le_men_im/pages/me/myQrCode.dart';
import 'package:flutter/material.dart';
import '../../models/index.dart';
import '../../constants.dart' show Constants;

class MyDetailPage extends StatelessWidget {
  static const QR_CODE_PREV_SIZE = 20.0;
  final User user;
  MyDetailPage({this.user});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        iconTheme: Theme.of(context).iconTheme,
        title: Text(
          '个人信息',
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 8.0),
        child: Column(
          children: <Widget>[
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(builder: (context) => ModifyMyAvatar()),
                );
              },
              title: Text('头像', ),
              trailing: trailWidget(ClipRRect(
                borderRadius: BorderRadius.circular(Constants.AvatarRadius),
                child: myAvatar(Global.profile.user.portrait, size: 50.0),
              )),
            ),
            Divider(
              height: 0.0,
            ),
            ListTile(
              title: Text('名字', ),
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new ModifyMyNickName(
                          displayName: Global.profile.user.displayName)),
                );
              },
              trailing: trailWidget(Text(Global.profile.user.displayName)),
            ),
            Divider(
              height: 0.0,
            ),
            ListTile(
              title: Text('手机号', ),
              trailing: Text(user.mobile),
            ),
            Divider(
              height: 0.0,
            ),
            ListTile(
              onTap: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(builder: (context) => new MyQrCode()),
                );
              },
              title: Text('我的二维码', ),
              trailing: trailWidget(Image.asset(
                'assets/images/ic_qrcode_preview_tiny.png',
                width: QR_CODE_PREV_SIZE,
                height: QR_CODE_PREV_SIZE,
              )),
            ),
            Divider(
              height: 0.0,
            ),
          ],
        ),
      ),
    );
  }
}
