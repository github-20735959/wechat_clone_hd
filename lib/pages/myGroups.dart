import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';
import '../constants.dart' show AppColors, AppStyles, Constants;

class MyGroups extends StatefulWidget {
  @override
  _MyGroupsState createState() => _MyGroupsState();
}

class _MyGroupsState extends State<MyGroups> {
  final AsyncMemoizer _memoizer = AsyncMemoizer();
  getMyGroups() {
    return _memoizer.runOnce(() async {
      var result = await FlutterWildfire.getMyGroups();
      return result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('群聊', style: AppStyles.TitleStyle),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(
                  IconData(
                    0xe64c,
                    fontFamily: Constants.IconFontFamily,
                  ),
                  size: Constants.ActionIconSize + 4.0,
                  color: const Color(AppColors.ActionIconColor))),
          elevation: 0.0,
          brightness: Brightness.light,
          backgroundColor: const Color(AppColors.PrimaryColor),
        ),
        body: FutureBuilder(
          future: getMyGroups(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print(snapshot.data);
            return Container();
          },
        ));
  }
}
