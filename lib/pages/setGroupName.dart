import 'package:bai_le_men_im/models/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';

class SetGroupNamePage extends StatefulWidget {
  final Group group;
  SetGroupNamePage({this.group}) : assert(group != null);
  @override
  _SetGroupNamePageState createState() => _SetGroupNamePageState();
}

class _SetGroupNamePageState extends State<SetGroupNamePage> {
  TextEditingController keywordController;
  bool _showCloseBtn = false;

  keywordControllerChangedHandler() async {
    final keyword = keywordController.text.trim();
    this.setState(() {
      _showCloseBtn = keyword.isNotEmpty;
    });
  }

  @override
  void initState() {
    String alias = widget.group.name;
    keywordController = TextEditingController(text: alias);

    keywordController.addListener(keywordControllerChangedHandler);
    super.initState();
  }

  @override
  void dispose() {
    keywordController.dispose();
    super.dispose();
  }

  setGroupName() async {
    final keyword = keywordController.text.trim();
    try {
      // await FlutterWildfire.setGroupName(widget.user.uid, keyword);
      await FlutterWildfire.modifyGroupInfo(widget.group.target, 'Modify_Group_Name', keyword, [0]);
      print('设置群组名成功');
      widget.group.name = keyword;
      // widget.user.friendAlias = keyword;
    } catch (e) {
      print('设置好友备注名失败');
      print(e);
    } finally {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          backgroundColor: Colors.grey[200],
          elevation: 0.0,
          centerTitle: true,
          iconTheme: Theme.of(context).iconTheme,
          title: Text(
            '修改群名称',
            style: TextStyle(color: Colors.black87),
          ),
          actions: <Widget>[
            MaterialButton(
              onPressed: setGroupName,
              child: Text('保存'),
            )
          ],
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: double.infinity,
              padding: EdgeInsets.only(top: 10.0, left: 10.0, bottom: 3.0),
              // color: Colors.grey,
              child: Text('群聊名称'),
            ),
            Container(
              color: Colors.white,
              padding: EdgeInsets.symmetric(horizontal: 12.0),
              child: Stack(
                children: <Widget>[
                  TextField(
                    autofocus: true,
                    controller: keywordController,
                    decoration: InputDecoration(border: InputBorder.none),
                  ),
                  _showCloseBtn
                      ? (Positioned(
                          right: 0.0,
                          // top: 5.0,
                          child: InkWell(
                            onTap: (){
                              keywordController.clear();
                            },
                            child: Container(
                            width: 40.0,
                            height: 40.0,
                            // color: Colors.red,
                            alignment: Alignment(0.0, 0.0),
                            child: CircleAvatar(
                              backgroundColor: Colors.grey,
                              child: Icon(
                                Icons.close,
                                size: 16.0,
                                color: Colors.white,
                              ),
                              radius: 9.0,
                            ),
                          ),
                          )
                          // IconButton(
                          //   icon: Icon(Icons.close),
                          //   onPressed: () {
                          //     keywordController.clear();
                          //   },
                          // ),
                          ))
                      : Container()
                ],
              ),
            ),
          ],
        ));
  }
}
