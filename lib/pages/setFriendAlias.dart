import 'package:bai_le_men_im/models/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';

class SetFriendAliasPage extends StatefulWidget {
  final User user;
  SetFriendAliasPage({this.user}) : assert(user != null);
  @override
  _SetFriendAliasPageState createState() => _SetFriendAliasPageState();
}

class _SetFriendAliasPageState extends State<SetFriendAliasPage> {
  TextEditingController keywordController;
  bool _showCloseBtn = false;

  keywordControllerChangedHandler() async {
    final keyword = keywordController.text.trim();
    this.setState(() {
      _showCloseBtn = keyword.isNotEmpty;
    });
  }

  @override
  void initState() {
    String alias =
        widget.user.friendAlias != null && widget.user.friendAlias.isNotEmpty
            ? widget.user.friendAlias
            : widget.user.displayName;
    keywordController = TextEditingController(text: alias);

    keywordController.addListener(keywordControllerChangedHandler);
    super.initState();
  }

  @override
  void dispose() {
    keywordController.dispose();
    super.dispose();
  }

  setFriendAlias() async {
    final keyword = keywordController.text.trim();
    try {
      await FlutterWildfire.setFriendAlias(widget.user.uid, keyword);
      print('设置好友备注名成功');
      widget.user.friendAlias = keyword;
    } catch (e) {
      print('设置好友备注名失败');
      print(e);
    } finally {
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          centerTitle: true,
          iconTheme: Theme.of(context).iconTheme,
          title: Text(
            '设置备注名',
            style: TextStyle(color: Colors.black87),
          ),
          actions: <Widget>[
            MaterialButton(
              onPressed: setFriendAlias,
              child: Text('保存'),
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('备注名'),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 12.0),
                child: Stack(
                  children: <Widget>[
                    TextField(
                      autofocus: true,
                      controller: keywordController,
                      decoration: InputDecoration(),
                    ),
                    _showCloseBtn
                        ? (Positioned(
                            right: 0.0,
                            child: IconButton(
                              icon: Icon(Icons.close),
                              onPressed: () {
                                keywordController.clear();
                              },
                            ),
                          ))
                        : Container()
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
