import 'package:flutter/material.dart';
import './cmomon/global.dart';

import './constants.dart' show AppColors, Routes;
import './home/home_screen.dart';
import './passport/mobile.dart';
import 'package:provider/provider.dart';
import './cmomon/userModel.dart';
import './cmomon/usersModel.dart';
import './cmomon/conversationList.dart';
import 'package:overlay_support/overlay_support.dart';

void main() {  
  Global.init().then((e) => runApp(MyApp())).catchError((error) {
    print('global.init error');
  });
}


class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: <SingleChildCloneableWidget>[
          ChangeNotifierProvider.value(
            value: UserModel(),
          ),
          ChangeNotifierProvider.value(
            value: ConversationModel(),
          ),
          ChangeNotifierProvider.value(
            value: UsersModel(),
          )
        ],
        child: OverlaySupport(
          child: MaterialApp(
            // showPerformanceOverlay: true, // TODO： 测试性能
            title: '数秩通信',
            theme: ThemeData.light().copyWith(
              primaryColor: Color(AppColors.PrimaryColor),
              cardColor: const Color(AppColors.CardBgColor),
              backgroundColor: Color(AppColors.BackgroundColor),
            ),
            home: HomeRoute(),
            // initialRoute: Routes.Entry,
            // initialRoute: Routes.PassportMobile,
            routes: {
              //   Routes.Entry: (context) => HomeScreen(),
              //   Routes.PassportMobile: (context) => PassportMobile(),
              //   Routes.Home: (context) => HomeScreen(),
              // Routes.Conversation: (context) => ConversationDetailPage(),
              //   'HomePageScreen': (context) => HomeScreen(),
            },
          ),
        ));
  }
}

class HomeRoute extends StatefulWidget {
  @override
  _HomeRouteState createState() => _HomeRouteState();
}

class _HomeRouteState extends State<HomeRoute> {
  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }

  Widget _buildBody() {
    UserModel userModel = Provider.of<UserModel>(context);
    print('userModel.isLogin = ' + userModel.isLogin.toString());
    if (!userModel.isLogin) {
      //用户未登录，显示登录按钮
      return PassportMobile();
    } else {
      //已登录，则展示项目列表
      return HomeScreen();
    }
  }
}
