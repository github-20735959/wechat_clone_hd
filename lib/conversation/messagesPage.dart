import 'dart:convert';
import 'package:bai_le_men_im/cmomon/global.dart';
import 'package:bai_le_men_im/conversation/conversation_detail_page.dart';
import 'package:bai_le_men_im/pages/sendRedEnvelope.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import '../conversation/inputPanel/entry.dart';
import '../constants.dart' show AppColors, AppStyles, Constants;
import 'package:flutter_wildfire/flutter_wildfire.dart';
import 'package:intl/intl.dart';
import '../models/index.dart';
import 'package:flutter_sound/flutter_sound.dart';
import '../models/index.dart' show MessageTypeRedEnvelope;
import './conversationItemContent.dart';
import 'dart:convert' show jsonDecode;

const int CountdownNum = 10;
const int pageSize = 20;
final DateFormat formatter = new DateFormat('MM-DD HH:mm');

FlutterSound flutterSound = new FlutterSound();

class MessagesPage extends StatefulWidget {
  final int messageId;
  final User user;
  final Function clearMessagesHandler;
  MessagesPage({@required this.user, this.messageId, this.clearMessagesHandler});
  @override
  _MessagesPageState createState() => _MessagesPageState();
}

class _MessagesPageState extends State<MessagesPage> {
  ScrollController _scrollController = new ScrollController();
  String targetId;
  List msgList;
  bool isRefreshing = false;
  bool isNoMoreData = false;
  bool isFetching = false;
  String conversationType;
  bool hasChangedConversation = false;

  getMessages(int fromIndex, int count) async {
    if (isFetching) return;
    if (isNoMoreData) return;
    isFetching = true;
    msgList = msgList == null ? [] : (msgList.length > 0 ? msgList : []);
    var result = await FlutterWildfire.getMessages(
        conversationType, targetId, 0, fromIndex, true, count, null);
    // print(result);
    if (result != null && result.length > 0) {
      msgList.addAll(List.generate(result.length, (index) {
        var _obj = result[result.length - 1 - index];
        return _obj;
      }));
      isFetching = false;
      isNoMoreData = result.length < pageSize;
      this.setState(() {});
      // _scrollController.
      return;
    }
    // print('从本地没找到，从远程找');
    // result = await FlutterWildfire.getRemoteMessages(
    //     conversationType, targetId, 0, fromIndex, count);
    // if (result != null && result.length > 0) {
    //   msgList.addAll(List.generate(result.length, (index) {
    //     var _obj = result[result.length - 1 - index];
    //     return _obj;
    //   }));
    //   isFetching = false;
    //   isNoMoreData = result.length < pageSize;
    //   this.setState(() {});
    //   return;
    // }

    isFetching = false;
    isNoMoreData = true;
    // print('从远程没有找到');
    this.setState(() {});
    return;
  }

  initPage() {
    // int _messageId = widget.messageId == null ? 0 : widget.messageId;
    int _messageId = 0;
    getMessages(_messageId, pageSize);
  }

  sendMessageOnProgress(dynamic event) async {
    // print('sendMessageOnProgress');
    // print(event);
    dynamic message = event['message'];
    message['total'] = event['total'];
    message['uploaded'] = event['uploaded'];
    int index =
        msgList.indexWhere((item) => item['messageId'] == message['messageId']);
    if (index > -1) {
      msgList[index] = message;
      // this.setState(() {
      //   msgList[index] = message;
      // });
    } else {
      this.setState(() {
        msgList.insert(0, message);
      });
    }
  }

  sendMessageOnMediaUpload(dynamic event) async {
    dynamic message = event['message'];
    message['remoteUrl'] = event['remoteUrl'];
    int index =
        msgList.indexWhere((item) => item['messageId'] == message['messageId']);
    if (index > -1) {
      msgList[index] = message;
      // this.setState(() {
      //   msgList[index] = message;
      // });
    } else {
      this.setState(() {
        msgList.insert(0, message);
      });
    }
  }

  sendMessageOnPrepareHandler(dynamic message) async {
    print('sendMessageOnPrepareHandler');
    print(message);
    int index =
        msgList.indexWhere((item) => item['messageId'] == message['messageId']);
    if (index > -1) {
      msgList[index] = message;
      // this.setState(() {
      //   msgList[index] = message;
      // });
    } else {
      this.setState(() {
        msgList.insert(0, message);
      });
    }
  }

  receiveMessagesHandler(dynamic messages) async {
    var message = messages[0];
    receiveMessageHandler(message, false);
  }

  receiveMessageHandler(dynamic message, bool clearUnRead) {
    String otherSideUserId = message['conversation']['target'];
    // print(ModalRoute.of(context).isActive);
    // print(ModalRoute.of(context).isCurrent);
    // print(ModalRoute.of(context).isFirst);
    // List list = ModalRoute.of(context).overlayEntries;
    // print(list);
    if (otherSideUserId != targetId) {
      if (ModalRoute.of(context).isCurrent) {
        print('收到别人的短信后的弹窗');
      }
      return;
    }
    if (message['type'] != null && message['type'] == 'TypingMessageContent') {
      // TODO: 输入状态
      // reGetCountdown();
      return;
    } else if (message['type'] != null &&
        (message['type'] == 'TextMessageContent' ||
            message['type'] == 'ImageMessageContent' ||
            message['type'] == 'SoundMessageContent' ||
            message['type'] == 'RedEnvelopeMessageContent')) {
      // compute(calculationNewMessages2, {'msgList': msgList, 'message': message})
      //     .then((_msgList) {
      //       print(_msgList);
      //   // this.setState(() {
      //   //   msgList = _msgList;
      //   // });
      // });
      // // calculationNewMessages2({'msgList': msgList, 'message': message}).then((_msgList) {
      // //   this.setState(() {
      // //     msgList = _msgList;
      // //   });
      // // });
      recevieSigleMessage(message);
    } else if (message['type'] != null &&
        (message['type'] == 'RedEnvelopeMessageContent')) {
    } else {
      print('有可能是别的类型的消息2');
      print('会话详情里面 receiveMessage handler');
    }
  }

  static Future<List<dynamic>> calculationNewMessages2(Map payload) async {
    String direction = "Receive";
    var msgList = payload['msgList'];
    var message = payload['message'];
    if (message['sender'] == Global.profile.user.uid) {
      direction = "Send";
    }
    msgList.insert(0, {
      "content": message['content'],
      "conversation": message['conversation'],
      "direction": direction,
      "messageId": message['messageId'],
      "messageUid": message['messageUid'],
      "sender": message['sender'],
      "serverTime": message['serverTime'],
      "status": message['status']
    });
    return msgList;
  }

  recevieSigleMessage(message) {
    String direction = "Receive";
    if (message['sender'] == Global.profile.user.uid) {
      direction = "Send";
    }
    msgList.insert(0, {
      "content": message['content'],
      "conversation": message['conversation'],
      "direction": direction,
      "messageId": message['messageId'],
      "messageUid": message['messageUid'],
      "sender": message['sender'],
      "serverTime": message['serverTime'],
      "status": message['status']
    });

    this.setState(() {});
  }

  @override
  void initState() {
    super.initState();
    targetId = widget.user.uid;
    conversationType = 'Single';
    print('message Page initState');
    initPage();
    _scrollController.addListener(_scrollHanlder);
    FlutterWildfire.on("receiveMessage", receiveMessagesHandler);
    // FlutterWildfire.on("sendMessageOnProgress", sendMessageOnProgress);
    // FlutterWildfire.on("sendMessageOnMediaUpload", sendMessageOnMediaUpload);
    FlutterWildfire.on("sendMessageOnPrepare", sendMessageOnPrepareHandler);
  }

  @override
  void dispose() {
    FlutterWildfire.off("sendMessageOnPrepare", sendMessageOnPrepareHandler);
    // FlutterWildfire.off("sendMessageOnProgress", sendMessageOnProgress);
    // FlutterWildfire.off("sendMessageOnMediaUpload", sendMessageOnMediaUpload);
    FlutterWildfire.off("receiveMessage", receiveMessagesHandler);
    _scrollController.removeListener(_scrollHanlder);
    _scrollController.dispose();
    super.dispose();
  }

  _scrollHanlder() {
    if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent &&
        !this.isRefreshing &&
        !this.isNoMoreData) {
      // print('滑动到最底部了');
      getMessages(msgList.last['messageId'], pageSize);
    }
  }
  
  clearMessagesHandler() async {
    await FlutterWildfire.clearMessages(conversationType, targetId, 0);
    
    this.setState((){
      isNoMoreData = true;
      msgList.clear();
    });
    if (widget.clearMessagesHandler != null) {
      widget.clearMessagesHandler();
    }
  }

  _onSendMsgHandler(payload, String type) async {
    var result;
    if (type == 'text') {
      try {
        result = await FlutterWildfire.sendTextMessage(
            conversationType, targetId, 0, null, payload);
      } catch (e) {} finally {}
    } else if (type == 'image') {
      try {
        result = await FlutterWildfire.sendImgMessage(
            conversationType, targetId, 0, null, payload);
      } catch (e) {
        print('发送图片文件错误');
        print(e);
      }
    } else if (type == 'audio') {
      try {
        result = await FlutterWildfire.sendAudioMessage(conversationType,
            targetId, 0, payload['path'], payload["duration"]);
      } catch (e) {
        print('发送语音文件错误');
        print(e);
      }
    } else if (type == 'RedEnvelope') {
      Navigator.push(
        context,
        new MaterialPageRoute(
            builder: (context) => new SendRedEnvelopePage(targetId: targetId)),
      );
    } else {
      print('未知的发送类型');
    }
    if (result != null) {
      int index = msgList
          .indexWhere((item) => item['messageId'] == result['messageId']);
      if (index > -1) {
        // msgList[index] = result;
        this.setState(() {
          msgList[index] = result;
        });
      } else {
        this.setState(() {
          msgList.insert(0, result);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final String displayName =
        widget.user.friendAlias != null && widget.user.friendAlias.isNotEmpty
            ? widget.user.friendAlias
            : widget.user.displayName;
    return Scaffold(
        appBar: AppBar(
          title: Text(displayName, style: AppStyles.TitleStyle),
          leading: IconButton(
              onPressed: () {
                Navigator.pop(context, msgList.length > 0 ? msgList.first : null);
              },
              icon: Icon(
                  IconData(
                    0xe64c,
                    fontFamily: Constants.IconFontFamily,
                  ),
                  size: Constants.ActionIconSize + 4.0,
                  color: const Color(AppColors.ActionIconColor))),
          elevation: 0.0,
          brightness: Brightness.light,
          backgroundColor: const Color(AppColors.PrimaryColor),
          actions: [
            IconButton(
                onPressed: () {
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (context) {
                    return ConversationDetailPage(
                        clearMessagesHandler: clearMessagesHandler,
                        singleUser: widget.user);
                  }));
                },
                icon: Icon(
                    IconData(
                      0xe609,
                      fontFamily: Constants.IconFontFamily,
                    ),
                    size: Constants.ActionIconSize + 4.0,
                    color: const Color(AppColors.ActionIconColor))),
            SizedBox(width: 16.0)
          ],
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: Container(
                width: double.infinity,
                color: const Color(AppColors.BackgroundColor),
                child: msgList != null
                    ? ListView.builder(
                        reverse: true,
                        controller: _scrollController,
                        itemBuilder: (BuildContext context, int index) {
                          if (index == msgList.length) {
                            if (isNoMoreData) {
                              return Container(
                                  // child: Text('没有更多了'),
                                  );
                            }
                            return Center(
                              child: SizedBox(
                                width: 30.0,
                                height: 30.0,
                                child: CircularProgressIndicator(
                                    // strokeWidth: 2.0,
                                    ),
                              ),
                            );
                          }
                          var item = msgList[index];

                          if (item['content']['content'] != null ||
                              item['content']['mediaType'] != null) {
                            String portrait;
                            if (item['direction'] == 'Send') {
                              portrait = Global.profile.user.portrait;
                            } else {
                              portrait = widget.user.portrait;
                            }

                            if (item['content']['content'] != null &&
                                item['content']['type'] == 1001) {
                              try {
                                MessageTypeRedEnvelope payload;
                                if (item['content']['content']
                                    is MessageTypeRedEnvelope) {
                                  payload = item['content']['content'];
                                } else {
                                  payload = MessageTypeRedEnvelope.fromJson(
                                      jsonDecode(item['content']['content']));
                                }
                                item['content']['content'] = payload;
                              } catch (e) {
                                return Container();
                              }
                            }
                            return Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: ConversationItem(
                                data: item,
                                avatalUrl:
                                    (portrait == null || portrait.isEmpty)
                                        ? 'assets/images/default_nor_avatar.png'
                                        : portrait,
                                displayName: '',
                              ),
                            );
                          } else if (item['content']['tip'] != null) {
                            return Center(
                              child: Container(
                                margin: EdgeInsets.all(8.0),
                                padding: EdgeInsets.symmetric(
                                    horizontal: 6.0, vertical: 8.0),
                                decoration: BoxDecoration(
                                    color: Colors.grey[400],
                                    borderRadius: BorderRadius.circular(3.0)),
                                child: Text(
                                  item['content']['tip'],
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            );
                          } else {
                            print('未知类型2');
                            // TODO： 未知类型
                            print(item);
                            return Container(
                              child: Text('未知'),
                            );
                          }
                        },
                        itemCount: msgList.length + 1)
                    : Column(
                        children: <Widget>[
                          SizedBox(
                            height: 20.0,
                          ),
                          CircularProgressIndicator()
                        ],
                      ),
              ),
            ),
            InputPanel(onSendMsgHandler: _onSendMsgHandler)
          ],
        ));
  }
}

class ConversationItem extends StatelessWidget {
  final String displayName;
  final String avatalUrl;
  final data;
  ConversationItem({@required this.data, this.avatalUrl, this.displayName});

  bool isAvatarFromNet() {
    if (this.avatalUrl.indexOf('http') == 0 ||
        this.avatalUrl.indexOf('https') == 0) {
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    Widget avatar;
    if (this.isAvatarFromNet()) {
      avatar = Image.network(
        this.avatalUrl,
        width: Constants.ConversationAvatarSize,
        height: Constants.ConversationAvatarSize,
        fit: BoxFit.cover,
      );
    } else {
      avatar = Image.asset(
        this.avatalUrl,
        width: Constants.ConversationAvatarSize,
        height: Constants.ConversationAvatarSize,
        fit: BoxFit.cover,
      );
    }
    avatar = ClipRRect(
      borderRadius: BorderRadius.circular(Constants.AvatarRadius),
      child: avatar,
    );
    if (data['direction'] == 'Receive') {
      return Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            this.avatalUrl != null
                ? avatar
                : CircleAvatar(
                    child: this.displayName != null
                        ? Text(this.displayName.substring(0, 1))
                        : Container(),
                  ),
            ConversationItemContent(
                payload: data,
                flutterSound: flutterSound,
                content: data['content'],
                messageId: data['messageId']),
            SizedBox(
              width: 35.0,
            )
          ],
        ),
      );
    } else if (data['direction'] == 'Send') {
      return Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: 35.0,
            ),
            SendStatus(data['status']),
            ConversationItemContent(
                payload: data,
                messageId: data['messageId'],
                flutterSound: flutterSound,
                reverse: true,
                color: Colors.green,
                content: data['content'],
                uploadProgress: data['remoteUrl'] != null ? 0.0 : 1.0),
            avatar,
          ],
        ),
      );
    } else {
      print(data);
      return Text('未知的方向， 或者未知数据类型');
    }
  }
}
