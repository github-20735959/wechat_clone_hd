import 'package:bai_le_men_im/cmomon/utils.dart';
import 'package:bai_le_men_im/conversation/chooseFriends.dart';
import 'package:bai_le_men_im/pages/search/keywordsInMessage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';
import '../constants.dart';
import '../models/index.dart';

class ConversationDetailPage extends StatefulWidget {
  final User singleUser;
  final Function clearMessagesHandler;
  ConversationDetailPage({this.singleUser, this.clearMessagesHandler});
  @override
  _ConversationDetailPageState createState() => _ConversationDetailPageState();
}

class _ConversationDetailPageState extends State<ConversationDetailPage> {
  String targetType;
  String target;
  bool _isTop = false;
  bool _isSilent = false;

  setConversationTop(bool newValue) async {
    await FlutterWildfire.setConversationTop(targetType, target, 0, newValue);
    setState(() {
      _isTop = newValue;
    });
  }

  setConversationSilent(bool newValue) async {
    await FlutterWildfire.setConversationSilent(
        targetType, target, 0, newValue);
    setState(() {
      _isSilent = newValue;
    });
  }

  getConversationInfo() async {
    var result = await FlutterWildfire.getConversation(targetType, target, 0);
    print(result);
    _isSilent = result['isSilent'];
    _isTop = result['isTop'];
  }

  @override
  initState() {
    super.initState();
    targetType = 'Single';
    target = widget.singleUser.uid;
    getConversationInfo();
  }

  showClearMessageMenu() async {
    return showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) {
        return CupertinoActionSheet(
          actions: <Widget>[
            CupertinoActionSheetAction(
              isDestructiveAction: true,
              child: Text('清空聊天记录'),
              onPressed: () async {
                if (widget.clearMessagesHandler != null) {
                  await widget.clearMessagesHandler();
                  Navigator.of(context).pop();
                }
              },
            ),
          ],
          cancelButton: CupertinoActionSheetAction(
            isDefaultAction: true,
            child: Text('取消'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: Theme.of(context).iconTheme,
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          '聊天详情',
          style: TextStyle(color: Colors.black87),
        ),
      ),
      body: ListView(children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Column(
                  children: <Widget>[
                    ClipRRect(
                        borderRadius:
                            BorderRadius.circular(Constants.AvatarRadius),
                        child:
                            myAvatar(widget.singleUser.portrait, size: 50.0)),
                    SizedBox(
                      height: 5.0,
                    ),
                    SizedBox(
                      width: 40.0,
                      child: Text(
                        widget.singleUser.displayName,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 12.0),
                      ),
                    )
                  ],
                ),
              ),
              SizedBox(
                width: 20.0,
              ),
              InkWell(
                onTap: () async {
                  await Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (context) {
                    return ChooseFriendPage(
                      excludeUsers: [widget.singleUser],
                    );
                  }));
                },
                child: Container(
                  width: 50.0,
                  height: 50.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      border: Border.all(
                        color: Colors.grey[500],
                        width: 1.0,
                      )),
                  alignment: Alignment(0.0, 0.0),
                  child: Icon(
                    Icons.add,
                    color: Colors.grey[500],
                    size: 30.0,
                  ),
                ),
              )
            ],
          ),
        ),
        SizedBox(
          height: 10.0,
          child: Container(
            color: Colors.grey[300],
          ),
        ),
        Divider(
          height: 0.0,
        ),
        ListTile(
          onTap: () async {
            await Navigator.of(context)
                .push(new MaterialPageRoute(builder: (context) {
              return SearchKeywordInConversation(targetObj: widget.singleUser);
            }));
          },
          title: Text('查找聊天内容'),
          trailing: Icon(Icons.chevron_right),
        ),
        Divider(
          height: 0.0,
        ),
        SizedBox(
          height: 10.0,
          child: Container(
            color: Colors.grey[300],
          ),
        ),
        (_isSilent is bool) ? ListTile(
          title: Text('消息免打扰'),
          trailing: CupertinoSwitch(
              value: _isSilent, onChanged: setConversationSilent),
        ) : Container() ,
        Padding(
          padding: EdgeInsets.only(left: 20.0),
          child: Divider(
            height: 0.0,
          ),
        ),
        (_isTop is bool) ? 
        ListTile(
          title: Text('聊天置顶'),
          trailing:
              CupertinoSwitch(value: _isTop, onChanged: setConversationTop),
        ) : Container(),
        Divider(
          height: 0.0,
        ),
        SizedBox(
          height: 10.0,
          child: Container(
            color: Colors.grey[300],
          ),
        ),
        ListTile(
          onTap: showClearMessageMenu,
          title: Text('清空聊天内容'),
        ),
        Divider(
          height: 0.0,
        ),
        SizedBox(
          height: 10.0,
          child: Container(
            color: Colors.grey[300],
          ),
        ),
      ]),
    );
  }
}
