import 'package:flutter/material.dart';
import '../../constants.dart' show emojis;

class InputTypeEmoji {
  String content;
  InputTypeEmoji(this.content);
}

class InputEmojis extends StatelessWidget {
  final Function onTap;
  InputEmojis({this.onTap});
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(8.0),
        child: GridView.count(
          crossAxisCount: 7,
          crossAxisSpacing: 2.0,
          mainAxisSpacing: 2.0,
          children: emojis.map((emoji) {
            if (emoji == "delete") {
              return IconButton(
                icon: Icon(Icons.keyboard_backspace),
                onPressed: () {
                  onTap(InputTypeEmoji(emoji));
                },
              );
            }
            return InkWell(
              splashColor: Colors.red,
              onTap: () {
                onTap(InputTypeEmoji(emoji));
              },
              child: Center(
                child: Container(
                  child: Text(emoji),
                ),
              ),
            );
          }).toList(),
        ));
  }
}
