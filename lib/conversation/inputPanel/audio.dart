import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';

import 'dart:async';
import 'package:flutter_sound/flutter_sound.dart';
import 'package:overlay_support/overlay_support.dart';

const distance = -18;

class InputAudioBtn extends StatefulWidget {
  final Function onSendAudioMsg;
  InputAudioBtn({this.onSendAudioMsg});
  @override
  _InputAudioBtnState createState() => _InputAudioBtnState();
}

class _InputAudioBtnState extends State<InputAudioBtn> {
  FlutterSound flutterSound = new FlutterSound();
  String path;
  bool _isRecording = false;
  int _duration = 0;
  final key = ValueKey('cancelTips');
  OverlaySupportEntry overlay;
  double firstPositionY;
  bool _overlayCancel = false;
  StreamSubscription _dbPeakSubscription;

  StreamSubscription _recorderSubscription;
  double _dbLevel;

  @override
  void initState() {
    initializeDateFormatting();
    // flutterSound.setDbLevelEnabled(true);
    super.initState();
  }

  @override
  void dispose() {
    try {
      flutterSound.stopPlayer();
      flutterSound.stopRecorder();
    } finally {
      super.dispose();
    }
  }

  void startRecorder() async {
    try {
      path = await flutterSound.startRecorder(null);
      print('startRecorder: $path');
      _duration = 0;
      _recorderSubscription = flutterSound.onRecorderStateChanged.listen((e) {
        _duration = e.currentPosition ~/ 1000;
      });
      // TODO: 声音强度
      // _dbPeakSubscription =
      //     flutterSound.onRecorderDbPeakChanged.listen((value) {
      //       print("got update -> $value");
      //       setState(() {
      //         this._dbLevel = value;
      //       });
      //     });

      this.setState(() {
        this._isRecording = true;
      });
    } catch (err) {
      print('startRecorder error: $err');
    }
  }

  showOverLay(bool _showInitStr) {
    overlay = showOverlay((context, t) {
      return Opacity(
        opacity: t,
        child: Material(
          color: Colors.black.withAlpha(80),
          child: Container(
            child: Center(
              child: Container(
                  width: 200.0,
                  height: 200.0,
                  decoration: BoxDecoration(
                      color: Colors.black.withAlpha(180),
                      borderRadius: BorderRadius.circular(10.0)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        _showInitStr ? Icons.mic : Icons.undo,
                        size: 80.0,
                        color: Colors.white,
                      ),
                      SizedBox(height: 20.0),
                      Container(
                          padding: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                              color: _showInitStr
                                  ? Colors.black.withAlpha(100)
                                  : Colors.redAccent.withAlpha(100),
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Text(
                            _showInitStr ? '手指上滑，取消' : '松开手指，取消',
                            style:
                                TextStyle(color: Colors.white, fontSize: 18.0),
                          ))
                    ],
                  )),
            ),
          ),
        ),
      );
    }, duration: Duration(microseconds: 0), key: const ValueKey('message'));
  }

  void stopRecorder(bool force) async {
    try {
      String result = await flutterSound.stopRecorder();
      print('stopRecorder: $result');
      if (force) {
      } else {
        if (_duration < 1) {
          toast('说话时间太短');
        } else {
          widget.onSendAudioMsg({"path": path, "duration": _duration});
        }
      }

      _duration = 0;
      if (_recorderSubscription != null) {
        _recorderSubscription.cancel();
        _recorderSubscription = null;
      }
      this.setState(() {
        this._isRecording = false;
      });
    } catch (err) {
      print('stopRecorder error: $err');
    }
  }

  @override
  Widget build(BuildContext context) {
    String title = (_isRecording ? '松开 结束' : '按住 说话'); // + _dbLevel.toString();
    return Listener(
      onPointerDown: (PointerDownEvent e) async {
        firstPositionY = e.localPosition.distance;
        _overlayCancel = false;
        if (!this._isRecording) {
          showOverLay(true);
          return this.startRecorder();
        }
        this.stopRecorder(false);
        overlay.dismiss();
      },
      onPointerUp: (PointerUpEvent e) {
        overlay.dismiss();
        stopRecorder(firstPositionY - e.localPosition.distance < distance);
      },
      onPointerMove: (PointerMoveEvent e) {
        if (firstPositionY - e.localPosition.distance < distance) {
          if (_overlayCancel) return;
          _overlayCancel = true;
          showOverLay(false);
          return;
        }
        if (!_overlayCancel) return;
        _overlayCancel = false;
        showOverLay(true);
        return;
      },
      child: Container(
        color: Colors.transparent,
        child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Center(
              child: Text(title),
            )),
      ),
    );
  }
}
