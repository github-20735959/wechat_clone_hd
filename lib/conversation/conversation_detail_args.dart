import 'package:flutter/material.dart';
import '../models/index.dart';

class ConversationDetailArgs {
  final User user;

  ConversationDetailArgs({
    @required this.user,
  });
}