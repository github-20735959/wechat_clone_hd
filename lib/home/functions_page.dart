import 'package:bai_le_men_im/pages/me/myDetail.dart';
import 'package:bai_le_men_im/pages/me/myScore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_wildfire/flutter_wildfire.dart';
import 'package:overlay_support/overlay_support.dart';
import '../cmomon/global.dart';
import '../app/app_home_page.dart';

import '../i18n//strings.dart' show Strings;

import '../modal/me.dart' show me;
import '../constants.dart' show AppColors, AppStyles, Constants;
import './full_width_button.dart';

import '../cmomon/userModel.dart';
import 'package:provider/provider.dart';

class _Header extends StatelessWidget {
  static const AVATAR_SIZE = 64.0;
  static const RADIUS = 6.0;
  static const SEPARATOR_SIZE = 16.0;
  static const QR_CODE_PREV_SIZE = 20.0;

  @override
  Widget build(BuildContext context) {
    return Consumer<UserModel>(
        builder: (BuildContext context, userModel, Widget child) {
      Widget avatar;
      if (userModel.user.portrait == null || userModel.user.portrait.isEmpty) {
        avatar = Image.asset(
          'assets/images/default_nor_avatar.png',
          width: AVATAR_SIZE,
          height: AVATAR_SIZE,
          fit: BoxFit.fill,
        );
      } else {
        avatar = Image.network(
          userModel.user.portrait,
          width: AVATAR_SIZE,
          height: AVATAR_SIZE,
          fit: BoxFit.cover,
        );
      }
      return FlatButton(
        onPressed: () {
          Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new MyDetailPage(user: userModel.user)),
          );
        },
        color: AppColors.HeaderCardBg,
        padding: const EdgeInsets.only(
            left: 32.0,
            right: FullWidthButton.HORIZONTAL_PADDING,
            top: 0.0,
            bottom: 42.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(RADIUS),
              child: avatar,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(
                        left: SEPARATOR_SIZE, bottom: 5.0),
                    child: Text(userModel.user.displayName,
                        style: AppStyles.HeaderCardTitleTextStyle),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: Container(
                        padding: const EdgeInsets.only(left: SEPARATOR_SIZE),
                        child: Text('手机号码：${userModel.user.mobile}',
                            style: AppStyles.HeaderCardDesTextStyle),
                      )),
                      Image.asset(
                        'assets/images/ic_qrcode_preview_tiny.png',
                        width: QR_CODE_PREV_SIZE,
                        height: QR_CODE_PREV_SIZE,
                      ),
                      FullWidthButton.arrowRight(),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      );
    });
  }
}

class FunctionsPage extends StatefulWidget {
  @override
  _FunctionsPageState createState() => _FunctionsPageState();
}

class _FunctionsPageState extends State<FunctionsPage> {
  static const SEPARATOR_SIZE = 10.0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(AppColors.BackgroundColor),
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _Header(),
            SizedBox(height: SEPARATOR_SIZE),
            FullWidthButton(
              iconPath: 'assets/images/ic_wallet.png',
              title: '积分',
              showDivider: true,
              onPressed: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new MyScorePage()),
                );
              },
            ),
            FullWidthButton(
              iconPath: 'assets/images/ic_collections.png',
              title: "轻应用",
              onPressed: () {
                Navigator.push(
                  context,
                  new MaterialPageRoute(
                      builder: (context) => new AppHomePage()),
                );
              },
            ),

            SizedBox(height: SEPARATOR_SIZE),
            // SizedBox(height: SEPARATOR_SIZE),
            // FullWidthButton(
            //   iconPath: 'assets/images/ic_collections.png',
            //   title: Strings.Collections,
            //   showDivider: true,
            //   onPressed: () {
            //     print('点击的是收藏按钮。');
            //   },
            // ),
            // FullWidthButton(
            //   iconPath: 'assets/images/ic_album.png',
            //   title: Strings.Album,
            //   showDivider: true,
            //   onPressed: () {
            //     print('点击的是相册按钮。');
            //   },
            // ),
            // FullWidthButton(
            //   iconPath: 'assets/images/ic_cards_wallet.png',
            //   title: Strings.Wallet,
            //   showDivider: true,
            //   onPressed: () {
            //     print('打开卡包应用');
            //   },
            // ),
            // FullWidthButton(
            //   iconPath: 'assets/images/ic_emotions.png',
            //   title: Strings.Emotions,
            //   onPressed: () {
            //     print('打开表情应用');
            //   },
            // ),
            // SizedBox(height: SEPARATOR_SIZE),
            FullWidthButton(
              iconPath: 'assets/images/ic_settings.png',
              title: Strings.Settings,
              des: Strings.AccountNotProtected,
              showRightArrow: false,
              onPressed: () {
                toast('功能暂时未开通');
              },
            ),
            SizedBox(height: SEPARATOR_SIZE),
            Container(
              width: double.infinity,
              height: FullWidthButton.HEIGHT,
              child: FlatButton(
                onPressed: () {
                  Global.profile.token = null;
                  FlutterWildfire.disconnect();
                  Provider.of<UserModel>(context).user = null;
                },
                padding:
                    EdgeInsets.only(left: FullWidthButton.HORIZONTAL_PADDING),
                color: Colors.white,
                child: Text('退出登陆', style: AppStyles.TitleStyle),
              ),
            )
          ],
        ),
      ),
    );
  }
}
